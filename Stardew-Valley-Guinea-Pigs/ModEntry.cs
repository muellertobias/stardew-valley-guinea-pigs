﻿using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stardew_Valley_Guinea_Pigs
{
    public partial class ModEntry : Mod
    {
        private string TexturePath;

        private GuineaPig guineaPig;

        public override void Entry(IModHelper helper)
        {
            TexturePath = helper.Content.GetActualAssetKey("assets/guinea-pig.png");
            helper.ConsoleCommands.Add("add_guinea_pig", "MEERSCHWEINCHEN ATTACKE", this.OnCommandReceived);
            helper.Events.GameLoop.DayStarted += OnDayStarted;
        }

        private void SpawnGuineaPig()
        {
            guineaPig = new GuineaPig(TexturePath);
            guineaPig.currentLocation = Game1.getFarm();
            guineaPig.currentLocation.addCharacter(guineaPig);
        }

        private void OnDayStarted(object sender, DayStartedEventArgs e)
        {
            if (Game1.MasterPlayer.mailReceived.Contains("HasGuineaPigPet"))
            {
                SpawnGuineaPig();
            }
        }

        private void OnCommandReceived(string command, string[] arguments)
        {
            if (guineaPig != null)
            {
                Monitor.Log("Your guinea pig don't like others...");
                return;
            }

            Game1.MasterPlayer.mailReceived.Add("HasGuineaPigPet");
        }
    }
}
