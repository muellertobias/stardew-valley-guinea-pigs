﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewValley;
using StardewValley.Characters;

namespace Stardew_Valley_Guinea_Pigs
{
    public class GuineaPig : Pet
    {
        public GuineaPig(string texturePath)
        {
            Name = "Meerschweinchen";
            displayName = Name;
            Position = new Vector2(52.5f, 8f) * 64f;
            Breather = false;
            willDestroyObjectsUnderfoot = false;
            Sprite = new AnimatedSprite(texturePath, 0, 16, 16);
        }

        public override void update(GameTime time, GameLocation location)
        {
            base.update(time, location);
            if (this.currentLocation == null)
            {
                this.currentLocation = location;
            }
            if (!Game1.eventUp && !Game1.IsClient)
            {
                CurrentBehavior = 0;
            }
        }

        public override void draw(SpriteBatch b)
        {
            Vector2 pos = Game1.GlobalToLocal(Game1.viewport, Position);

            b.Draw(Sprite.Texture, pos, Sprite.sourceRect, Color.White);
        }
    }
}
